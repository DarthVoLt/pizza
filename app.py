from flask import Flask, render_template, request, redirect, url_for, flash


app = Flask(__name__)
app.secret_key = b'\xc4^2l\xf1-\x10CY\xf7'

orders = []
i = 0
pizza = [
    {
        "id": 0,
        "title": "Маргарита",
        "ingredients": "тесто, помидоры, моцарелла, базилик, оливковое масло",
        "image": "https://tiktak-delivery.ru/img/dish/dxT8HziEidb8ii02.jpg?mark=%2Fhome%2Ftiktak%2Fhtdocs%2Fpublic%2"
                 "Fassets%2Findex%2Fimages%2Fwater-mark-2.png&s=d4d696a1a73ec9597267ab040b0995c3"
    },
    {
        "id": 1,
        "title": "Неаполитанская",
        "ingredients": "тесто, помидоры, моцарелла, пармезан, анчоусы, базилик",
        "image": "http://www.smachno.in.ua/files/2016/nesolodka-vypichka/pica-neapolitanska/pica-neapolitanska.jpg"
    },
    {
        "id": 2,
        "title": "Каприччоза",
        "ingredients": "тесто, моцарелла, помидоры, грибы, черные и зеленые оливки, артишоки, базили",
        "image": "https://www.bylena.ru/images/uploaded/600x_Pizza-Capriciosa-1256-step-7928.jpg"
    },
    {
        "id": 3,
        "title": "Прошутто",
        "ingredients": "тесто, моцарелла, ветчина, черный перец, оливковое масло",
        "image": "https://just-eat.by/image/data/shops/1072/3591.jpg"
    },
    {
        "id": 4,
        "title": "Дьябола",
        "ingredients": "тесто, салями и острый калабрийский перец",
        "image": "http://pizza-celentano.rovno.ua/upload/items/130-big-IMG_4218.jpg"
    },
    {
        "id": 5,
        "title": "Сицилийская",
        "ingredients": "тесто, сыр пекорино, томатный соус, анчоусы",
        "image": "http://photo.gurmanika.com/recipe/10-11/1/sicilijskaya-picca.jpg"
    }
]


@app.route('/')
def index():
    return render_template('pizza.html', pizza=pizza)


@app.route('/order', methods=['GET', 'POST'])
def cart():
    global i
    pizza_order = []
    qty_order = []
    if request.method == 'POST':
        for p in pizza:
            if 'pizza_name_' + str(p['id']) in request.form:
                pizza_order.append(p['title'])
                qty_order.append(request.form['qty_' + str(p['id'])])
        if not pizza_order:
            flash('Не выбрана не одна пицца')
            return redirect(url_for('cart'))
        elif not qty_order:
            flash('Не указанно количество пицц')
            return redirect(url_for('cart'))
        order = {
            'order_id': i,
            'pizza': pizza_order,
            'pizza_qty': qty_order,
            'client_name': request.form['client_name'],
            'client_phone': request.form['client_phone'],
            'client_address': request.form['client_address'],
            'order_state': 'Оформлен'
        }
        orders.append(order)
        i += 1

        return redirect(url_for('index'))
    else:
        return render_template('order.html', pizza=pizza)


@app.route('/pass', methods=['GET', 'POST'])
def password():
    access = 'ihatepizza'
    if request.method == 'POST':
        word = request.form['password']
        if word == access:
            return redirect(url_for('manager'))
        else:
            flash('Не верный пароль')

            return redirect(url_for('password'))
    else:
        return render_template('pass.html')


@app.route('/manager', methods=['GET', 'POST'])
def manager():
    if request.method == 'POST':
        for element in request.form:
            id_tuple = element.rpartition('_')
            element_id = int(id_tuple[2])
            if request.form[str(element)] != orders[element_id]['order_state']:
                orders[element_id]['order_state'] = request.form[str(element)]
        return render_template('pass.html')
    else:
        return render_template('manager.html', orders=orders)
